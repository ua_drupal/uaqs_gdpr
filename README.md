# UAQS GDPR Module

Packages various GDPR related contrib modules and configuration.

## Packaged Dependencies

When this module is used as part of a Drupal distribution (such as [UA Quickstart](https://bitbucket.org/ua_drupal/ua_quickstart)), the following
dependencies will be automatically packaged with the distribution.

### Drupal Contrib Modules
- [EU Cookie Compliance](https://www.drupal.org/project/eu_cookie_compliance)
- [General Data Protection Regulation](https://www.drupal.org/project/gdpr)
